package com.example.kissdigital.di.modules

import com.example.kissdigital.model.Character
import dagger.Module
import dagger.Provides

@Module
class CharacterModule(var character: Character) {

    @Provides
    fun provideCharacter(): Character{
        return character
    }
}