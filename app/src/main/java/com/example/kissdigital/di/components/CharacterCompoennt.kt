package com.example.kissdigital.di.components

import com.example.kissdigital.di.modules.CharacterModule
import com.example.kissdigital.modules.detail.RaMDetailFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [CharacterModule::class])
interface CharacterCompoennt {
    fun inject(fragment: RaMDetailFragment)

}