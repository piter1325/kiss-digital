package com.example.recruitmentttask.network

import com.example.kissdigital.network.Service
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class Client<T>() {

    val API_URL = "https://rickandmortyapi.com/api/"
    protected var gson = createGson()
    val service: Service

    fun createGson(): Gson {
        return GsonBuilder()
            .setDateFormat("yyyy-MM-dd HH:mm:ss")
            .create()
    }

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl(API_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(getMyHttpClient())
            .build()

        service = retrofit.create(Service::class.java)
    }

    fun getMyHttpClient(): OkHttpClient {

        return OkHttpClient.Builder()
            .readTimeout(90*1000,TimeUnit.MILLISECONDS)
            .connectTimeout(15 * 1000, TimeUnit.MILLISECONDS)
            .addNetworkInterceptor(getLoggingInterceptor())
//            .addInterceptor{getInterceptor()}
            .build()

    }

    private fun getLoggingInterceptor(): HttpLoggingInterceptor{
        return HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
    }
}