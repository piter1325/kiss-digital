package com.example.kissdigital.network

import android.content.Context
import com.example.kissdigital.model.Character
import com.example.kissdigital.model.CharactersResponse
import com.example.recruitmentttask.network.Client
import retrofit2.Call
import retrofit2.Callback

object Connection {

    var context: Context? = null
    var mClient: Client<Service>? = null

    fun init(context: Context) {
        this.context = context
        mClient = Client()
    }

    fun getCharacterList(page: Int,callback: Callback<CharactersResponse>) {
        val call: Call<CharactersResponse> = mClient!!.service.getCharacterList(page)
        call.enqueue(callback)
    }
}