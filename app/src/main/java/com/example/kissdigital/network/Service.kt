package com.example.kissdigital.network

import androidx.databinding.Observable
import com.example.kissdigital.model.Character
import com.example.kissdigital.model.CharactersResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface Service {

    @GET("character")
    fun getCharacterList(
        @Query("page") page: Int
    ): Call<CharactersResponse>
}