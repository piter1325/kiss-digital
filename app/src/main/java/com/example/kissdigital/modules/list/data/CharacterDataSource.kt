package com.example.kissdigital.modules.list.data

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.example.kissdigital.helpers.AlertDialogManager
import com.example.kissdigital.model.Character
import com.example.kissdigital.model.CharactersResponse
import com.example.kissdigital.network.Connection
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CharacterDataSource : PageKeyedDataSource<Int, Character>() {

    var state: MutableLiveData<State> = MutableLiveData()
    var pageCount: Int = 0

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, Character>) {
        updateState(State.LOADING)

        Connection.getCharacterList(1,object : Callback<CharactersResponse> {
            override fun onResponse(call: Call<CharactersResponse>, response: Response<CharactersResponse>) {
                updateState(State.DONE)
                val charactersResponse = response.body()
                val characterList = charactersResponse?.results
                pageCount = if(charactersResponse?.info?.pages == null) 0 else charactersResponse.info.pages

                if (response.code() == 200){
                    characterList?.let {
                        callback.onResult(characterList,null,2)
                    }
                }else{
                    AlertDialogManager.showErrorMessage(response.code().toString())
                }
            }
            override fun onFailure(call: Call<CharactersResponse>, t: Throwable) {
                updateState(State.ERROR)
                AlertDialogManager.showErrorMessage()
            }
        })
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Character>) {
        updateState(State.LOADING)

        Connection.getCharacterList(params.key,object : Callback<CharactersResponse> {
            override fun onResponse(call: Call<CharactersResponse>, response: Response<CharactersResponse>) {
                updateState(State.DONE)
                val charactersResponse = response.body()
                val characterList = charactersResponse?.results

                if (response.code() == 200){
                    characterList?.let {
                        var pageKey = params.key
                        if(pageKey < pageCount){
                            pageKey += 1
                        }else{
                            pageKey = null
                            updateState(State.END)
                        }
                        callback.onResult(charactersResponse.results, pageKey)
                    }
                }else{
                    AlertDialogManager.showErrorMessage(response.code().toString())
                }
            }

            override fun onFailure(call: Call<CharactersResponse>, t: Throwable) {
                updateState(State.ERROR)
                AlertDialogManager.showErrorMessage()
            }
        })
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Character>) {
    }

    private fun updateState(state: State) {
        this.state.postValue(state)
    }

}