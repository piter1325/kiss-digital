package com.example.kissdigital.modules.detail

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.example.kissdigital.model.Character

class RaMDetailViewModel: ViewModel() {

    val name = ObservableField<String>()
    val avatar = ObservableField<String>()
    val status = ObservableField<String>()
    val gender = ObservableField<String>()
    val origin = ObservableField<String>()
    val location = ObservableField<String>()

    fun setItem(character: Character){
        name.set(character.name)
        avatar.set(character.image)
        status.set(character.status)
        gender.set(character.gender)
        origin.set(character.origin.name)
        location.set(character.location.name)
    }
}