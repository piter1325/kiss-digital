package com.example.kissdigital.modules.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.kissdigital.MainActivity
import com.example.kissdigital.R
import com.example.kissdigital.databinding.ItemCharacterBinding
import com.example.kissdigital.di.components.DaggerCharacterCompoennt
import com.example.kissdigital.di.modules.CharacterModule
import com.example.kissdigital.model.Character
import com.example.kissdigital.modules.detail.RaMDetailFragment

class RaMListAdapter: PagedListAdapter<Character,RecyclerView.ViewHolder>(CHARACTER_COMPARATOR) {

    companion object {
        val CHARACTER_COMPARATOR = object : DiffUtil.ItemCallback<Character>() {
            override fun areItemsTheSame(oldItem: Character, newItem: Character): Boolean =
                oldItem.name == newItem.name

            override fun areContentsTheSame(oldItem: Character, newItem: Character): Boolean =
                oldItem.name == newItem.name
        }
    }

    private val DATA_VIEW_TYPE = 1
    private val FOOTER_VIEW_TYPE = 2

    var countModifier = 1

    override fun getItemViewType(position: Int): Int {
        return if (position < super.getItemCount()) DATA_VIEW_TYPE else FOOTER_VIEW_TYPE
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemView: View = LayoutInflater.from(parent.context).inflate(R.layout.item_character, parent, false)
        val viewModel = RaMItemViewModel()
        val binding = ItemCharacterBinding.bind(itemView)
        binding.viewModel = viewModel

        val loadingItemView: View = LayoutInflater.from(parent.context).inflate(R.layout.item_list_footer, parent, false)

        return if(viewType == DATA_VIEW_TYPE) ViewHolder(itemView,viewModel) else LoadingViewHolder(loadingItemView)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        if (position == itemCount - countModifier) return
        val item = getItem(position) ?: return
        if(holder is ViewHolder) {
            holder.setElement(item)

            holder.itemView.setOnClickListener { View ->
                val fragment = RaMDetailFragment.newInstance()
                val context = holder.itemView.context

                val characterCompoennt = DaggerCharacterCompoennt.builder()
                    .characterModule(CharacterModule(item)).build()
                characterCompoennt.inject(fragment)


                if (context is MainActivity) {
                    context.attach(fragment, RaMDetailFragment.TAG, true)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return if(super.getItemCount() == 0) 0 else super.getItemCount() + countModifier
    }

    class ViewHolder(itemView: View, val viewModel: RaMItemViewModel) : RecyclerView.ViewHolder(itemView){
        fun setElement(character: Character){
            viewModel.setItem(character)
        }
    }

    class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
    }

}