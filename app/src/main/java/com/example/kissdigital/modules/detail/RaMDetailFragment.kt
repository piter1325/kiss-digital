package com.example.kissdigital.modules.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.example.kissdigital.R
import com.example.kissdigital.base.BaseFragment
import com.example.kissdigital.databinding.FragmentCharacterDetailBinding
import com.example.kissdigital.model.Character
import javax.inject.Inject

class RaMDetailFragment: BaseFragment() {

    companion object{
        val TAG = "RaMDetailFragment"

        fun newInstance(): RaMDetailFragment{
            return RaMDetailFragment()
        }
    }

    @Inject
    lateinit var character: Character

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: FragmentCharacterDetailBinding = DataBindingUtil.inflate(inflater, getLayoutRes(), container, false)
        val viewModel = RaMDetailViewModel()

        binding.viewModel = viewModel

        viewModel.setItem(character)

        return binding.root
    }

    fun getLayoutRes(): Int {
        return R.layout.fragment_character_detail
    }

    override fun getBackPressType(): Int {
        return 1
    }

    override fun getHomeType(): Int {
       return 1
    }
}