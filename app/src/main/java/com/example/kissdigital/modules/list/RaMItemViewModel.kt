package com.example.kissdigital.modules.list

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.example.kissdigital.model.Character

class RaMItemViewModel: ViewModel(){

    val name = ObservableField<String>()
    val avatar = ObservableField<String>()


    fun setItem(character: Character){
        name.set(character.name)
        avatar.set(character.image)
    }
}