package com.example.kissdigital.modules.list.data

enum class State {
    DONE, LOADING, ERROR, END
}