package com.example.kissdigital.modules.list.data

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.example.kissdigital.model.Character

class CharacterDataFactory : DataSource.Factory<Int, Character>() {

    val characterDataSourceLiveData = MutableLiveData<CharacterDataSource>()

    override fun create(): DataSource<Int, Character> {
        val characterDataSource =
            CharacterDataSource()
        characterDataSourceLiveData.postValue(characterDataSource)
        return characterDataSource
    }
}