package com.example.kissdigital.modules.list


import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.example.kissdigital.model.Character
import com.example.kissdigital.modules.list.data.CharacterDataFactory
import com.example.kissdigital.modules.list.data.CharacterDataSource
import com.example.kissdigital.modules.list.data.State

class RaMListViewModel: ViewModel() {

    val hideText = ObservableField<Boolean>(true)
    val hideProgres = ObservableField<Boolean>(false)
    private val pageSize = 20

    var characterList: LiveData<PagedList<Character>>
    private var liveDataSource: LiveData<CharacterDataSource>
    private val itemDataSourceFactory: CharacterDataFactory =
        CharacterDataFactory()

    init {
        liveDataSource = itemDataSourceFactory.characterDataSourceLiveData

        val config = PagedList.Config.Builder()
            .setPageSize(pageSize)
            .setEnablePlaceholders(false)
            .build()

        characterList = LivePagedListBuilder(itemDataSourceFactory, config).build()
    }

    fun getState(): LiveData<State> = Transformations.switchMap<CharacterDataSource,
            State>(itemDataSourceFactory.characterDataSourceLiveData, CharacterDataSource::state)

    fun listIsEmpty(): Boolean {
        return characterList.value?.isEmpty() ?: true
    }


}