package com.example.kissdigital.modules.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.kissdigital.R
import com.example.kissdigital.base.BaseFragment
import com.example.kissdigital.databinding.FragmentRamListBinding
import com.example.kissdigital.modules.list.data.State


class RaMListFragment: BaseFragment() {

    companion object{
        val TAG = "RaMListFragment"

        fun newInstance(): RaMListFragment {
            return RaMListFragment()
        }
    }

    val adapter = RaMListAdapter()
    val viewModel = RaMListViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: FragmentRamListBinding = DataBindingUtil.inflate(inflater, getLayoutRes(), container, false)

        binding.viewModel = viewModel

        viewModel.characterList.observe(viewLifecycleOwner, Observer {
            adapter.submitList(it)
            adapter.notifyDataSetChanged()
        })
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val recyclerView = view.findViewById<RecyclerView>(R.id.recycler)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter

        initState()
    }

    private fun initState() {
        viewModel.getState().observe(this, Observer { state ->
            viewModel.hideProgres.set(!(viewModel.listIsEmpty() && state == State.LOADING))
            viewModel.hideText.set(!(viewModel.listIsEmpty() && state == State.ERROR))

            if(state == State.END) adapter.countModifier = 0
        })
    }

    fun getLayoutRes(): Int {
        return R.layout.fragment_ram_list
    }

    override fun getBackPressType(): Int {
        return 0
    }

    override fun getHomeType(): Int {
        return 0
    }
}