package com.example.kissdigital.model

class Character {

    lateinit var name: String
    lateinit var status: String
    lateinit var gender: String
    lateinit var image: String
    lateinit var origin: Location
    lateinit var location: Location
}