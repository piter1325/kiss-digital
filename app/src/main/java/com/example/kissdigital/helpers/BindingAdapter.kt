package com.example.kissdigital.helpers

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.example.kissdigital.R
import com.nostra13.universalimageloader.core.ImageLoader

@BindingAdapter("app:setImageLoader")
fun setImageLoader(imageView: ImageView, link: String?) {
    if (link != null && link.length > 1) {
        imageView.alpha = 0f
        ImageLoader.getInstance().displayImage(link, imageView)
        imageView.animate().alpha(1f).duration = 100
    } else {
        imageView.setImageDrawable(imageView.context.getDrawable(R.drawable.ic_user))
    }
}

@BindingAdapter("app:setViewInvisibly")
fun setImageLoader(view: View, isInvisibly: Boolean) {
    view.visibility = if (!isInvisibly) View.VISIBLE else View.GONE
}