package com.example.kissdigital.helpers

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import com.example.kissdigital.R
import com.example.kissdigital.base.BaseActivity

object AlertDialogManager {

    fun init(context: Context, activity: BaseActivity) {
        this.activity = activity
        this.context = context
    }

    private var dialog: AlertDialog? = null
    private lateinit var activity: BaseActivity
    private lateinit var context: Context


    fun show(message: String, clickListener: DialogInterface.OnClickListener?
    ) {
        dismiss()
        if (!activity.isFinishing) {
            dialog = AlertDialog.Builder(context, R.style.AlertsDialogTheme)
                .setMessage(message)
                .setPositiveButton(context.getString(R.string.ok), clickListener)
                .setNegativeButton(context.getString(R.string.cancel), getDefaultClick())
                .setCancelable(false)
                .show()
        } else activity.finish()
    }

    fun showErrorMessage() {
        showErrorMessage(context.getString(R.string.default_network_error))
    }

    fun showErrorMessage(message: String?) {
        dismiss()
        if (message == null || message.isEmpty()) return
        dialog = AlertDialog.Builder(context, R.style.AlertsDialogTheme)
            .setMessage(context.getString(R.string.network_error) + message)
            .setPositiveButton(context.getString(R.string.ok), getDefaultClick())
            .setCancelable(false)
            .show()
    }

    private fun getDefaultClick(): DialogInterface.OnClickListener {
        return DialogInterface.OnClickListener { dialogInterface: DialogInterface?, i: Int -> dismiss() }
    }

    fun dismiss() {
        if (dialog != null && dialog!!.isShowing) dialog!!.dismiss()
        dialog = null
    }
}