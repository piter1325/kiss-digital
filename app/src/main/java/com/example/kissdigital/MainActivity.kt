package com.example.kissdigital

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.example.kissdigital.base.BaseActivity
import com.example.kissdigital.databinding.ActivityMainBinding
import com.example.kissdigital.helpers.AlertDialogManager
import com.example.kissdigital.modules.list.RaMListFragment

class MainActivity : BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        DataBindingUtil.setContentView<ActivityMainBinding>(this, getLayoutResource())
        AlertDialogManager.init(this, this)
        setSupportActionBar(findViewById(R.id.toolbar))

        attach(RaMListFragment.newInstance(), RaMListFragment.TAG, true)
    }

    fun refreshHomeButton(){
        if(supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }
        setHomeButton()
    }

    fun getLayoutResource(): Int {
        return R.layout.activity_main
    }

    override fun getIdFragmentContainer(): Int {
        return R.id.main_fragment_id
    }
}