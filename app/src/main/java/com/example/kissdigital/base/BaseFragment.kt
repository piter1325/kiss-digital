package com.example.kissdigital.base

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import androidx.fragment.app.Fragment
import com.example.kissdigital.MainActivity
import com.example.kissdigital.R

abstract class BaseFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)

        menu.clear()
        (activity as? MainActivity)?.refreshHomeButton()
    }

    fun getArrowIcon(): Int {
        return R.drawable.ic_arrow_white
    }

    fun getCloseIcon(): Int {
        return R.drawable.ic_close_white
    }

    /*
        0 - finish()
        1 - onBackPress()
     */
    abstract fun getBackPressType(): Int

    /*
       0 - X
       1 - arrow
    */
    abstract fun getHomeType(): Int
}