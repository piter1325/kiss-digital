package com.example.kissdigital.base

import android.content.DialogInterface
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.kissdigital.R
import com.example.kissdigital.helpers.AlertDialogManager

abstract class BaseActivity : AppCompatActivity() {

    fun attach(fragment: Fragment?, tag: String?, addToBackStack: Boolean) {
        val fragmentManager = supportFragmentManager
        val ft = fragmentManager.beginTransaction()
        ft.addToBackStack(tag)
        if (addToBackStack) {
            ft.add(getIdFragmentContainer(), fragment!!, tag)
        } else {
            ft.replace(getIdFragmentContainer(), fragment!!)
        }
        ft.commit()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home){
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    fun getCurrentFragment(): BaseFragment{
        return (supportFragmentManager.findFragmentById(getIdFragmentContainer()) as BaseFragment?)!!
    }

    override fun onBackPressed() {
        val backPressType: Int
        val baseFragment: BaseFragment
        val fragment = supportFragmentManager.findFragmentById(getIdFragmentContainer())

        if (fragment is BaseFragment) {
            baseFragment = fragment
            backPressType = baseFragment.getBackPressType()
        } else {
            backPressType = 1
        }

        when (backPressType) {
            0 -> AlertDialogManager.show(getString(R.string.close_app), getCloseAppClickListener())
            1 -> {
                super.onBackPressed()
            }
        }
    }

    open fun setHomeButton() {
        when (getCurrentFragment().getHomeType()) {
            0 -> setHomeButton(getCurrentFragment().getCloseIcon())
            1 -> setHomeButton(getCurrentFragment().getArrowIcon())
        }
    }

    open fun setHomeButton(drawableRes: Int) {
        val drawable = resources.getDrawable(drawableRes)
        if (supportActionBar != null) {
            supportActionBar!!.setHomeAsUpIndicator(drawable)
        }
    }

    open fun getCloseAppClickListener(): DialogInterface.OnClickListener? {
        return DialogInterface.OnClickListener { dialog: DialogInterface?, which: Int -> finish() }
    }

    abstract fun getIdFragmentContainer(): Int
}