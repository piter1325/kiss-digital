package com.example.kissdigital

import android.app.Application
import com.example.kissdigital.network.Connection
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration

class KDApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        Connection.init(this)

        val defaultOptions: DisplayImageOptions = DisplayImageOptions.Builder()
            .cacheInMemory(true)
            .cacheOnDisk(true)
            .build()
        val config = ImageLoaderConfiguration.Builder(applicationContext)
                .defaultDisplayImageOptions(defaultOptions)
                .build()
        ImageLoader.getInstance().init(config)
    }
}